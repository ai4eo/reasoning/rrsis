# RRSIS


## Getting started
**Further details are available at the following GitHub repository**: https://github.com/zhu-xlab/rrsis

The RefSegRS dataset in the paper "RRSIS: Referring Remote Sensing Image Segmentation" can be downloaded from https://huggingface.co/datasets/JessicaYuan/RefSegRS


Some visualization examples of the dataset:

![Visualization examples](assets/rrsis.png 'Visualization examples of the RefSegRS dataset')


### If you find it useful, please kindly cite our paper.


```
@article{yuan2024rrsis,
  title={RRSIS: Referring Remote Sensing Image Segmentation},
  author={Yuan, Zhenghang and Mou, Lichao and Hua, Yuansheng and Zhu, Xiao Xiang},
  journal={IEEE Transactions on Geoscience and Remote Sensing},
  year={2024}
}
```



## License
CC-BY-4.0

