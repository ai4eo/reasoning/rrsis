import os
import sys
import torch.utils.data as data
import torch
from torchvision import transforms
from torch.autograd import Variable
import numpy as np
from PIL import Image
import torchvision.transforms.functional as TF
import random

from bert.tokenization_bert import BertTokenizer

import h5py
from refer.refer import REFER

from args import get_parser
import pickle

# Dataset configuration initialization
parser = get_parser()
args = parser.parse_args()


def build_rsris_batches(setname):
    im_dir1 = '/home/zhenghang/SECOND_train_set/im1/'
    im_dir2 = '/home/zhenghang/SECOND_train_set/im2/'
    seg_label_dir = '/home/zhenghang/SECOND_train_set/new_labels/'
    if setname == 'train':
        setfile = 'idx_tra_nosmall_v2.csv'
    if setname == 'val':
        setfile = 'idx_val_nosmall_v2.csv'
    if setname == 'test':
        setfile = 'idx_test_nosmall_v2.csv'

    label_all = 'label_all.csv'
    vocab_file = './data/vocabulary_Rsris.txt'

    n_batch = 0
    rfile = '/home/zhenghang/SECOND_train_set/label_all.csv'
    train_ids = []
    if True:
        tf = open('/home/zhenghang/SECOND_train_set/'+setfile,'r')
        lines = tf.readlines()
        for line in lines:
            if 'id' not in line:
                #train_ids.append(int(line[:-2]))
                train_ids.append(int(line))
    nn = 0
    imgnames = set()
    imname = 'start'
    all_imgs1 = []
    all_imgs2 = []
    all_labels = []
    all_sentences = []

    test_sentence = []

    with open(rfile,'r') as rf:
        rlines = rf.readlines()
        for idx,line in enumerate(rlines):
            if 'png' not in line:
                continue
            lsplit = line.split(',')
            if int(lsplit[0]) in train_ids:
                im_name1 = im_dir1 + lsplit[1]
                im_name2 = im_dir2 + lsplit[1]
                seg = seg_label_dir + lsplit[0] + '.png'

                #for sentence in (lsplit[2], lsplit[3]):
                test_sentence.append(lsplit[2])
                for sentence in [lsplit[3]]:
                    sent = sentence
                    im_1 = im_name1
                    im_2 = im_name2
                    label_mask = seg
                    all_imgs1.append(im_name1)
                    all_imgs2.append(im_name2)
                    all_labels.append(label_mask)
                    all_sentences.append(sent)

    print("Dataset Loaded.")
    with open('phrase_texts.pickle', 'wb') as handle:
        pickle.dump(test_sentence, handle, protocol=pickle.HIGHEST_PROTOCOL)
    #with open('imgs.pickle', 'wb') as handle:
    #    pickle.dump(all_imgs1, handle, protocol=pickle.HIGHEST_PROTOCOL)
    return all_imgs1,all_imgs2,all_labels,all_sentences



class ReferDataset(data.Dataset):

    def __init__(self,
                 args,
                 image_transforms=None,
                 target_transforms=None,
                 split='train',
                 eval_mode=False):

        self.classes = []
        self.image_transforms = image_transforms
        self.target_transform = target_transforms
        self.split = split
        self.max_tokens = 20

        all_imgs1, all_imgs2, all_labels, all_sentences = build_rsris_batches(self.split)
        self.sentences = all_sentences
        self.imgs1 = all_imgs1
        self.imgs2 = all_imgs2
        self.labels = all_labels

        self.input_ids = []
        self.attention_masks = []
        self.tokenizer = BertTokenizer.from_pretrained(args.bert_tokenizer)

        self.eval_mode = eval_mode
        # if we are testing on a dataset, test all sentences of an object;
        # o/w, we are validating during training, randomly sample one sentence for efficiency
        for r in range(len(self.imgs1)):
            img_sentences = [self.sentences[r]]
            sentences_for_ref = []
            attentions_for_ref = []

            for i, el in enumerate(img_sentences):
                sentence_raw = el
                attention_mask = [0] * self.max_tokens
                padded_input_ids = [0] * self.max_tokens

                input_ids = self.tokenizer.encode(text=sentence_raw, add_special_tokens=True)

                # truncation of tokens
                input_ids = input_ids[:self.max_tokens]

                padded_input_ids[:len(input_ids)] = input_ids
                attention_mask[:len(input_ids)] = [1]*len(input_ids)

                sentences_for_ref.append(torch.tensor(padded_input_ids).unsqueeze(0))
                attentions_for_ref.append(torch.tensor(attention_mask).unsqueeze(0))

            self.input_ids.append(sentences_for_ref)
            self.attention_masks.append(attentions_for_ref)

    def get_classes(self):
        return self.classes

    def __len__(self):
        return len(self.imgs1)

    def __getitem__(self, index):
        this_img1 = self.imgs1[index]
        this_img2 = self.imgs2[index]

        img1 = Image.open(this_img1).convert("RGB")
        img2 = Image.open(this_img2).convert("RGB")
        label_mask = Image.open(self.labels[index]).convert('L')

        ref_mask = np.array(label_mask) > 0
        annot = np.zeros(ref_mask.shape)
        annot[ref_mask == 1] = 1

        annot = Image.fromarray(annot.astype(np.uint8), mode="P")

        if self.image_transforms is not None:
            # resize, from PIL to tensor, and mean and std normalization
            img1, target = self.image_transforms(img1, annot)
            img2, target = self.image_transforms(img2, annot)

        if self.eval_mode:
            embedding = []
            att = []
            for s in range(len(self.input_ids[index])):
                e = self.input_ids[index][s]
                a = self.attention_masks[index][s]
                embedding.append(e.unsqueeze(-1))
                att.append(a.unsqueeze(-1))

            tensor_embeddings = torch.cat(embedding, dim=-1)
            attention_mask = torch.cat(att, dim=-1)
        else:
            choice_sent = np.random.choice(len(self.input_ids[index]))
            tensor_embeddings = self.input_ids[index][choice_sent]
            attention_mask = self.attention_masks[index][choice_sent]

        #return img1, img2, target, tensor_embeddings, attention_mask, this_img1
        return img1, target, tensor_embeddings, attention_mask
